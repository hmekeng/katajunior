﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoneBookApp.WEB.Models
{
    public class Contact
    {
        public int ContactID { get; set; }
        [Required(ErrorMessage = "First Name is required", AllowEmptyStrings = false)]
        [StringLength(100)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required", AllowEmptyStrings = false)]
        [StringLength(100)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Phone number is required", AllowEmptyStrings = false)]
        [StringLength(30)]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        public Contact() { }
        //constructeur
        public Contact(int contactid, string firstname, string lastname,string phonenumber)
        {
            this.ContactID = contactid;
            this.FirstName = firstname;
            this.LastName = lastname;
            this.PhoneNumber = phonenumber;
           
        }
    }
}